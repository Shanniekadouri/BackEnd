package com.bethashanti.beckend.dao;

import com.bethashanti.beckend.objects.Instructor;
import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
@Scope(value = "singleton")
@Repository
public interface InstructorDao extends CrudRepository<Instructor, Long> {}
