package com.bethashanti.beckend.dao;

import com.bethashanti.beckend.objects.Child;
import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
@Scope(value = "singleton")
@Repository
public interface ChildDao extends CrudRepository<Child, Long> {}

//package com.bethashanti.beckend.dao;
//
//import com.bethashanti.beckend.EnumTypes;
//import com.bethashanti.beckend.connector.Connector;
//import com.bethashanti.beckend.objects.Child;
//
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.List;
//
//import com.bethashanti.beckend.objects.ChildTest;
//import com.bethashanti.beckend.utils.HibernateUtil;
//import org.hibernate.Session;
//import org.hibernate.Transaction;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Scope;
//import org.springframework.data.jpa.provider.HibernateUtils;
//import org.springframework.jdbc.core.BeanPropertyRowMapper;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.jdbc.core.RowMapper;
//import org.springframework.stereotype.Component;
//import org.springframework.stereotype.Repository;
//import org.springframework.transaction.annotation.Transactional;
//
//import javax.persistence.PersistenceUnit;
//import javax.sql.DataSource;
//
//@Component
//@Scope(value = "singleton")//i think we should change it to singleton and not prototype
//@Repository
//@Transactional
//@PersistenceUnit
//public class ChildDao {
//
//
//    private Connector connector;
//    private DataSource dataSource;
//
//    @Autowired
//    public ChildDao(Connector connector) {
//        this.connector = connector;
//        dataSource = connector.getDataSource();
//    }
//
//    public void setDataSource(DataSource dataSource) {
//        this.dataSource = dataSource;
//    }
//
//    public boolean checkIfExsits(String id) {
//        JdbcTemplate template = new JdbcTemplate(dataSource);
//        String sql = "SELECT count(*) from child where id_number = ?";
//
//        int count = template.queryForObject(sql, new Object[] {id}, Integer.class);
//
//        return count > 0;
//    }
//
//    public boolean save(Child child) {
//
//        JdbcTemplate template = new JdbcTemplate(dataSource);
//
//
//        int row = template.update("insert into child(id_number, first_name, last_name, house," +
//                "arrival_date, gender, date_of_birth, city, address, instructor, status," +
//                "phone_number, email, mothers_name, fathers_name," +
//                        "num_of_siblings, economic_status, origin, nation, place_of_birth," +
//                        "passport_number, citizenship)" +
//                "values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
//                ,child.getIdNumber(), child.getFirstName(), child.getLastName(), child.getHouse().toString(),
//                child.getArrivalDate(), child.getGender().toString(), child.getDateOfBirth(),
//                child.getCity(), child.getAddress(), child.getStaffInCharge(), child.getStatus().toString(),
//                child.getPhoneNum(), child.geteMail(), child.getMothersName(), child.getFathersName(),
//                child.getNumOfSiblings(), child.getEconomicStatus().toString(),
//                child.getOrigin().toString(), child.getNation().toString(), child.getPlaceOfBirth(),
//                child.getPassportNum(), child.getCitizenship());
//
//        return row > 0;
//    }
//
//    public boolean delete(String id) {
//        JdbcTemplate template = new JdbcTemplate(dataSource);
//        String sql = "delete from child where id_number = ?";
//        int row = template.update(sql, id);
//
//        return row > 0;
//    }
//
//    public Child getChildById(String id) {
//        JdbcTemplate template = new JdbcTemplate(dataSource);
//        String sql = "select * from child where id_number=?";
//        return template.queryForObject(sql, new Object[]{id}, new BeanPropertyRowMapper<Child>(Child.class));
//    }
//
//    public List<Child> getChildren() {
//        JdbcTemplate template = new JdbcTemplate(dataSource);
//        return template.query("select * from child", new RowMapper<Child>() {
//            public Child mapRow(ResultSet rs, int row) throws SQLException {
//                Child c = new Child();
//                c.setIdNumber(rs.getString(1));
//                c.setFirstName(rs.getString(2));
//                c.setLastName(rs.getString(3));
//                c.setHouse(Enum.valueOf(EnumTypes.House.class, rs.getString(4)));
//                c.setArrivalDate(rs.getDate(5));
//                c.setGender(Enum.valueOf(EnumTypes.Sex.class, rs.getString(6)));
//                c.setDateOfBirth(rs.getDate(7));
//                c.setAddress(rs.getString(8));
//                c.setStaffInCharge(rs.getString(9));
//                c.setStatus(Enum.valueOf(EnumTypes.Status.class, rs.getString(10)));
//                c.setPhoneNum(rs.getString(11));
//                c.seteMail(rs.getString(12));
//                c.setMothersName(rs.getString(13));
//                c.setFathersName(rs.getString(14));
//                c.setNumOfSiblings(rs.getString(15));
//                c.setEconomicStatus(Enum.valueOf(EnumTypes.EconomicStatus.class, rs.getString(16)));
//                c.setOrigin(Enum.valueOf(EnumTypes.Origin.class, rs.getString(17)));
//                c.setNation(Enum.valueOf(EnumTypes.Nation.class, rs.getString(18)));
//                c.setPlaceOfBirth(rs.getString(19));
//                c.setPassportNum(rs.getString(20));
//                c.setCitizenship(rs.getString(21));
//
//                return c;
//            }
//        });
//    }
//
//    public void saveChildTest(ChildTest childTest) {
//        Transaction transaction = null;
//        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
//            // start a transaction
//            transaction = session.beginTransaction();
//            // save the child objects
//            session.save(childTest);
//            // commit transaction
//            transaction.commit();
//        } catch (Exception e) {
//            if (transaction != null) {
//                transaction.rollback();
//            }
//            e.printStackTrace();
//        }
//
//    }
//}