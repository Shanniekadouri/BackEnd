package com.bethashanti.beckend;

public class EnumTypes {

    public enum Sex {
        female,
        male,
        unknown
    }

    public enum House {
        Tel_Aviv,
        Midbar,
        GeneralHouse
    }

    public enum Status {
        living,
        other
    }

    public enum ParentsStatus {
        other,
        married,
        divorced
    }

    public enum EconomicStatus {
        average,
        low,
        high
    }

    public enum Origin {
        israeli,
        other,
        unknown
    }

    public enum Nation {
        jewish,
        muslim,
        christian,
        beduin,
        other,
        unknown
    }

    public enum Restriction {
        smoking,
        other,
        none
    }

    public enum Urgency {
        one,
        two,
        three,
        four,
        five
    }

    public enum Topic {
        manager,
    }

    public enum ShirtSize {
        XXL,
        XL,
        L,
        M,
        S,
        XS,
        unknown
    }

    public enum Position{
        Management,
        Coordinator,
        House_Coordinator,
        Instructor,
        Operations,
        other
    }

    public enum EmploymentStatus{
        Active,
        Left,
        On_Hold
    }

    public enum YesNoOption{
        Yes,
        No
    }

    public enum TaskTopic{
        Health_Care,
        Education,
        Social,
        Legal,
        Management_Tracking,
        Responses,
        Circle,
        General
    }

}