package com.bethashanti.beckend.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.bethashanti.beckend.objects.Child;
import com.bethashanti.beckend.objects.HttpResponse;
import com.bethashanti.beckend.services.ChildService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class ChildController {

    @Autowired
    private ChildService childService;

    @RequestMapping(value = "/ChildGenerator", method = RequestMethod.GET)
    public HttpResponse ChildGenerator(@Valid @ModelAttribute("child") Child child,
                                 BindingResult bindingResult) {

        HttpResponse httpResponse;
        List<String> messages = new ArrayList<String>();

        if (bindingResult.hasErrors()) {
            List<FieldError> fieldErrors = bindingResult.getFieldErrors();
            for (FieldError error : fieldErrors ) {
                messages.add(error.getField());
            }

            httpResponse = new HttpResponse(HttpStatus.BAD_REQUEST, messages, child);
            return httpResponse;
        }

        Child savedChild = childService.saveChild(child);
        if(savedChild == null) {
            messages.add("Internal problem");
            httpResponse = new HttpResponse(HttpStatus.INTERNAL_SERVER_ERROR, messages, savedChild);
            return httpResponse;
        }

        messages.add("Child saved");
        httpResponse = new HttpResponse(HttpStatus.OK, messages, savedChild);
        return httpResponse;
    }

    @RequestMapping(value = "/ChildGetter", method = RequestMethod.GET)
    public Optional<Child> ChildGetter(Long id) {

        Optional<Child> child = childService.getChildById(id);

        return child;
    }
    @RequestMapping(value = "/getAllChild", method = RequestMethod.GET)
    public Iterable<Child> getAllInstructors() {

        Iterable<Child> child =  childService.getAllChild();

        return child;
    }

    @RequestMapping(value = "/DeleteChildById", method = RequestMethod.POST)
    public void DeleteInstructorById(Long id) {

        childService.deleteChild(id);

    }
}

//package com.bethashanti.beckend.controllers;
//
//import java.util.List;
//import java.util.concurrent.atomic.AtomicLong;
//
//import com.bethashanti.beckend.objects.Child;
//import com.bethashanti.beckend.EnumTypes;
//import com.bethashanti.beckend.objects.ChildTest;
//import com.bethashanti.beckend.services.ChildService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.server.ResponseStatusException;
//
//import java.util.Date;
//import java.text.SimpleDateFormat;
//
//
//@RestController
//public class ChildController {
//
//    private static final String template = "Hello, %s!";// by marking %s, we can use String format to insert a String to replace %s
//    private final AtomicLong counter = new AtomicLong();//atomic increment of long value
//
//    @Autowired
//    private ChildService childService;
//
//    /*
//    when a request is mapped to "/greeting" this controller is triggered and runs
//
//    @RequestParam binds the value of the query string parameter name into the name parameter of the greeting() method.
//     If the name parameter is absent in the request, the defaultValue of "World" is used
//    */
//    @RequestMapping("/greetings")
//    public HttpStatus greeting(@RequestParam(value = "ID", defaultValue = "") String idNumber,
//                               @RequestParam(value = "arrivalDate", defaultValue = "") String arrivaleDateStr,
//                               @RequestParam(value = "firstName", defaultValue = "") String firstName,
//                               @RequestParam(value = "lastName", defaultValue = "") String lastName,
//                               @RequestParam(value = "gender", defaultValue = "unknown") EnumTypes.Sex gender,
//                               @RequestParam(value = "dateOfBirth", defaultValue = "") String dateOfBirthStr,
//                               @RequestParam(value = "mothersName", defaultValue = "") String mothersName,
//                               @RequestParam(value = "fathersName", defaultValue = "") String fathersName,
//                               @RequestParam(value = "house", defaultValue = "GeneralHouse") EnumTypes.House house,
//                               @RequestParam(value = "city", defaultValue = "") String city,
//                               @RequestParam(value = "address", defaultValue = "") String address,
//                               @RequestParam(value = "staffInCharge", defaultValue = "") String staffInCharge,
//                               @RequestParam(value = "status", defaultValue = "other") EnumTypes.Status status,
//                               @RequestParam(value = "phoneNum", defaultValue = "") String phoneNum,
//                               @RequestParam(value = "eMail", defaultValue = "") String eMail,
//                               @RequestParam(value = "numOfSiblings", defaultValue = "0") String numOfSiblings,
//                               @RequestParam(value = "economicStatus", defaultValue = "average") EnumTypes.EconomicStatus economicStatus,
//                               @RequestParam(value = "origin", defaultValue = "unknown") EnumTypes.Origin origin,
//                               @RequestParam(value = "nation", defaultValue = "unknown") EnumTypes.Nation nation,
//                               @RequestParam(value = "placeOfBirth", defaultValue = "") String placeOfBirth,
//                               @RequestParam(value = "passportNum", defaultValue = "") String passportNum,
//                               @RequestParam(value = "citizenship", defaultValue = "") String citizenship) {
//
//        HttpStatus httpStatus = HttpStatus.OK;
//
//        if (idNumber.length() != 9) {
//            httpStatus = HttpStatus.BAD_REQUEST;
//            //throw new IllegalArgumentException("Invalid ID");
//            throw new ResponseStatusException(httpStatus, "Invalid id");
//        }
//
//        Date arrivaleDate = null;
//        Date dateOfBirth = null;
//
//        try {
//            Date today = new Date();
//
//            if(!arrivaleDateStr.equals("")) {
//                arrivaleDate = new SimpleDateFormat("yyyy-MM-dd").parse(arrivaleDateStr);
//                if (arrivaleDate.after(today)) {
//                    httpStatus = HttpStatus.BAD_REQUEST;
//                    throw new ResponseStatusException(httpStatus, "Invalid date");
//                }
//            }
//            if(!dateOfBirthStr.equals("")) {
//                dateOfBirth = new SimpleDateFormat("yyyy-MM-dd").parse(dateOfBirthStr);
//                if(dateOfBirth.after(today)) {
//                    httpStatus = HttpStatus.BAD_REQUEST;
//                    //throw new IllegalArgumentException("Invalid date");
//                    throw new ResponseStatusException(httpStatus, "Invalid date");
//                }
//            }
//        }
//        catch(Exception e) {
//            httpStatus = HttpStatus.BAD_REQUEST;
//            //throw new IllegalArgumentException("Invalid date");
//            throw new ResponseStatusException(httpStatus, "Invalid date");
//        }
//
//
//        Child child = new Child(this.counter.incrementAndGet(), house, idNumber, arrivaleDate, firstName, lastName, gender, dateOfBirth,
//                city, address, staffInCharge, status, phoneNum, eMail, mothersName, fathersName ,numOfSiblings,
//                economicStatus, origin, nation, placeOfBirth, passportNum, citizenship);
//
//        ChildTest childTest = new ChildTest(firstName, lastName);
//        //childService.saveChildTest(childTest);
//
//        if (!childService.checkIfChildExists(child)) {
//            boolean check = childService.saveChild(child);
//            if(!check) {
//                httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
//                //throw new RuntimeException("Could not insert into table");
//                throw new ResponseStatusException(httpStatus, "Could not insert into table");
//            }
//        }
//        else {
//            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
//            //throw new RuntimeException("Child already exists in the system");
//            throw new ResponseStatusException(httpStatus, "Child already exists in the system");
//        }
//
//
//        return httpStatus;
//    }
//
//    @RequestMapping("/delete")
//    public HttpStatus delete(@RequestParam(value = "ID", defaultValue = "") String idNumber,
//                               @RequestParam(value = "firstName", defaultValue = "") String firstName,
//                               @RequestParam(value = "lastName", defaultValue = "") String lastName) {
//
//        HttpStatus httpStatus = HttpStatus.OK;
//
//        if(!childService.deleterChile(idNumber)) {
//            httpStatus = HttpStatus.BAD_REQUEST;
//            throw new RuntimeException("Could not remove child");
//        }
//
//        return httpStatus;
//    }
//
//    @RequestMapping("/getChildren")
//    public List<Child> getChildren() {
//
//        return childService.getChildren();
//
//    }
//
//
//}