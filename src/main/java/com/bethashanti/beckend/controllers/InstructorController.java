package com.bethashanti.beckend.controllers;

import java.util.Optional;
import com.bethashanti.beckend.objects.Instructor;
import com.bethashanti.beckend.services.InstructorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
public class InstructorController {

    @Autowired
    private InstructorService instructorService;


    @RequestMapping(value = "/InstructorGenerator", method = RequestMethod.GET)
    public Instructor InstructorGenerator(Instructor instructor) {

        instructorService.saveInstructor(instructor);

        return  instructor;
    }

    @RequestMapping(value = "/InstructorGetter", method = RequestMethod.GET)
    public Optional<Instructor> InstructorGetter(Long id) {

        Optional<Instructor> instructor =  instructorService.getInstructorById(id);

        return  instructor;
    }
    @RequestMapping(value = "/getAllInstructors", method = RequestMethod.GET)
    public Iterable<Instructor> getAllInstructors() {

        Iterable<Instructor> instructor =  instructorService.getAllInstructors();

        return  instructor;
    }

    @RequestMapping(value = "/DeleteInstructorById", method = RequestMethod.POST)
    public void DeleteInstructorById(Long id) {

       instructorService.deleteInstructor(id);

    }
}



//    @RequestMapping(value = "/InstructorGenerator",
//                    method = RequestMethod.GET)
//    public Instructor InstructorGenerator(@RequestParam(value = "firstName", defaultValue = "") String firstName,
//                                          @RequestParam(value = "lastName", defaultValue = "") String lastName,
//                                          @RequestParam(value = "idNumber", defaultValue = "") Long idNumber,
//                                          @RequestParam(value = "house", defaultValue = "GeneralHouse") EnumTypes.House house,
//                                          @RequestParam(value = "gender", defaultValue = "unknown") EnumTypes.Sex gender,
//                                          @RequestParam(value = "dateOfEmployment", defaultValue = "") String dateOfEmployment,
//                                          @RequestParam(value = "dateOfBirth", defaultValue = "") String dateOfBirth,
//                                          @RequestParam(value = "shirtSize", defaultValue = "unknown") EnumTypes.ShirtSize shirtSize,
//                                          @RequestParam(value = "position", defaultValue = "other") EnumTypes.Position position,
//                                          @RequestParam(value = "isShantiTherapyInstructor", defaultValue = "No") EnumTypes.YesNoOption isShantiTherapyInstructor,
//                                          @RequestParam(value = "cellphoneNum", defaultValue = "") String cellphoneNum,
//                                          @RequestParam(value = "homePhoneNum", defaultValue = "") String homePhoneNum,
//                                          @RequestParam(value = "eMail", defaultValue = "") String eMail,
//                                          @RequestParam(value = "homeAddress", defaultValue = "") String homeAddress,
//                                          @RequestParam(value = "educationType", defaultValue = "") String educationType,
//                                          @RequestParam(value = "hasALicense ", defaultValue = "No") EnumTypes.YesNoOption hasALicense ,
//                                          @RequestParam(value = "licenseDescription", defaultValue = "") String licenseDescription,
//                                          @RequestParam(value = "employmentStatus", defaultValue = "Active") EnumTypes.EmploymentStatus employmentStatus,
//                                          @RequestParam(value = "dateOfUnemployment", defaultValue = "") String dateOfUnemployment,
//                                          @RequestParam(value = "unemploymentReason", defaultValue = "") String unemploymentReason,
//                                          @RequestParam(value = "isManagement ", defaultValue = "No") EnumTypes.YesNoOption isManagement ,
//                                          @RequestParam(value = "hasCriminalRecord ", defaultValue = "No") EnumTypes.YesNoOption hasCriminalRecord ,
//                                          @RequestParam(value = "hasSexOffenderRecord  ", defaultValue = "No") EnumTypes.YesNoOption hasSexOffenderRecord){
//        Instructor instructor =    new Instructor(firstName, lastName, idNumber, house, gender, dateOfEmployment,
//                dateOfBirth, shirtSize, position, isShantiTherapyInstructor, cellphoneNum, homePhoneNum, eMail, homeAddress,
//                educationType, hasALicense, licenseDescription, employmentStatus, dateOfUnemployment, unemploymentReason,
//                isManagement, hasCriminalRecord, hasSexOffenderRecord);
//        instructorService.saveInstructor(instructor);
//
//        return  instructor;
//    }

//    @RequestMapping("/")
//    public String home(){
//        return "homeNF";
//    }