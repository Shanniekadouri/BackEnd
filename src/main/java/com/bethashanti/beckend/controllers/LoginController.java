package com.bethashanti.beckend.controllers;

import com.bethashanti.beckend.dao.UserDao;
import com.bethashanti.beckend.objects.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class LoginController {

	@GetMapping("login")
	public String login() {

		return "login";

	}

	@GetMapping("homeNF")
	public String showHomePage() {

		return "homeNF";

	}
	@GetMapping("userDetails")
	public String userDetails() {

		return "userDetails";

	}
}
