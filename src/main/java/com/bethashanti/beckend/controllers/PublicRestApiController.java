package com.bethashanti.beckend.controllers;

import com.bethashanti.beckend.dao.UserDao;
import com.bethashanti.beckend.objects.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("test")
public class PublicRestApiController {
    private UserDao userDao;

    public PublicRestApiController(UserDao userDao){
        this.userDao = userDao;
    }

    @GetMapping("test1")
    public String test1(){
        return "API Test 1";
    }

    @GetMapping("test2")
    public String test2(){
        return "API Test 2";
    }

    @GetMapping("users")
    public List<User> users(){
        return this.userDao.findAll();
    }

}