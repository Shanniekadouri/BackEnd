package com.bethashanti.beckend.objects;

import com.bethashanti.beckend.EnumTypes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;
import java.util.*;

public class Task {
    private static final AtomicLong internalCounter = new AtomicLong();
    private long systemId;
    private List<Child> relevantChildren;
    private EnumTypes.House house;
    private Date dateOfCreation;
    private Instructor composer;
    private Date complitionDate;
    private EnumTypes.TaskTopic taskTopic;
    private String taskContent;
    private Instructor instructorIncharge;
    private boolean taskCompletion;



    public Task(List<String> relevantChildren, String house, String dateOfCreation, String composer, String taskTopic, String taskContent, String instructorIncharge, String complitionDate, String taskCompletion){
        this.systemId = Task.internalCounter.incrementAndGet();
        this.relevantChildren = getChildrenList(relevantChildren);
        this.house = EnumTypes.House.valueOf(house);
        if(dateOfCreation.equals("")){
           // this.dateOfCreation = new SimpleDateFormat("dd/MM/yyyy").parse(new Date().toString());
        }else {
            this.dateOfCreation = parseDate(dateOfCreation);
        }
        this.composer = getInstructorByName(composer);
        if(complitionDate.equals("")){
            this.complitionDate = null;
        }else {
            this.complitionDate = parseDate(taskCompletion);
        }
        this.taskTopic = EnumTypes.TaskTopic.valueOf(taskTopic);
        this.taskContent = taskContent;
        this.instructorIncharge = getInstructorByName(instructorIncharge);
        this.taskCompletion = evalYesNoOption(EnumTypes.YesNoOption.valueOf(taskCompletion));

    }


    private boolean evalYesNoOption(EnumTypes.YesNoOption valueOfInput) {
        boolean returnValue = false;
        if(valueOfInput == EnumTypes.YesNoOption.Yes){
            returnValue = true;
        }
        return  returnValue;
    }

    private Instructor getInstructorByName(String composer) {
        return null;
    }

    private List<Child> getChildrenList(List<String> relevantChildren) {
        return null;
    }

    private Date parseDate(String dateToParse) {
        Date returnDate = null;
        if((dateToParse.equals("") == false)){
            try {
                returnDate = (new SimpleDateFormat("yyyy-MM-dd")).parse(dateToParse);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return returnDate;
    }
}
