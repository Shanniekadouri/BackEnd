package com.bethashanti.beckend.objects;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "child_test")
public class ChildTest {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    private int test;
    
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "first_name")
    private String firstName;

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "last_name")
    private String lastName;

    public ChildTest(String firstName, String lastName) {
        //this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public ChildTest() {
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }



    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
