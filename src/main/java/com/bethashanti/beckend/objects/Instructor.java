package com.bethashanti.beckend.objects;
import com.bethashanti.beckend.EnumTypes;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@Getter // to use: enable annotation processing
@Setter // to use: enable annotation processing
@Entity
@Table(name = "instructor")
public class Instructor {
    @Id
    @Column(name = "id_number")
    private Long idNumber;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "house")
    private EnumTypes.House house;
    @Column(name = "gender")
    private EnumTypes.Sex gender;
    @Column(name = "date_of_employment")
    private Date dateOfEmployment;
    @Column(name = "date_of_birth")
    private Date dateOfBirth;
    @Column(name = "shirt_size")
    private EnumTypes.ShirtSize shirtSize;
    @Column(name = "position")
    private EnumTypes.Position position;
    @Column(name = "is_shanti_therapy_instructor")
    private EnumTypes.YesNoOption isShantiTherapyInstructor;
    @Column(name = "cell_phone_num")
    private String cellphoneNum;
    @Column(name = "home_phone_num")
    private String homePhoneNum;
    @Column(name = "eMail")
    private String eMail;
    @Column(name = "home_address")
    private String homeAddress;
    @Column(name = "education_type")
    private String educationType;
    @Column(name = "has_a_license")
    private EnumTypes.YesNoOption hasALicense;
    @Column(name = "license_description")
    private String licenseDescription;
    @Column(name = "employment_status")
    private EnumTypes.EmploymentStatus employmentStatus;
    @Column(name = "date_of_unemployment")
    private Date dateOfUnemployment;
    @Column(name = "unemployment_reason")
    private String unemploymentReason;
    @Column(name = "is_management")
    private EnumTypes.YesNoOption isManagment;
    @Column(name = "has_criminal_record")
    private EnumTypes.YesNoOption hasCriminalRecord;
    @Column(name = "has_sex_offender_record")
    private EnumTypes.YesNoOption hasSexOffenderRecord;

    public Instructor() {}

    public Instructor(String firstName, String lastName, Long idNumber, EnumTypes.House house, EnumTypes.Sex gender, String dateOfEmployment,
                      String dateOfBirth, EnumTypes.ShirtSize shirtSize, EnumTypes.Position position, EnumTypes.YesNoOption isShantiTherapyInstructor, String cellphoneNum,
                      String homePhoneNum, String eMail, String homeAddress, String educationType, EnumTypes.YesNoOption hasALicense, String licenseDescription,
                      EnumTypes.EmploymentStatus employmentStatus, String dateOfUnemployment, String unemploymentReason, EnumTypes.YesNoOption isManagment,
                      EnumTypes.YesNoOption hasCriminalRecord, EnumTypes.YesNoOption hasSexOffenderRecord) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.idNumber = idNumber;
        this.house = house;
        this.gender = gender;
        this.dateOfEmployment = parseDate(dateOfEmployment);
        this.dateOfBirth = parseDate(dateOfBirth);
        this.shirtSize = shirtSize;
        this.position = position;
        this.isShantiTherapyInstructor = isShantiTherapyInstructor;
        this.cellphoneNum = cellphoneNum;
        this.homePhoneNum = homePhoneNum;
        this.eMail = eMail;
        this.homeAddress = homeAddress;
        this.educationType = educationType;
        this.hasALicense = hasALicense;
        this.licenseDescription = licenseDescription;
        this.employmentStatus = employmentStatus;
        this.dateOfUnemployment = parseDate(dateOfUnemployment);
        this.unemploymentReason = unemploymentReason;
        this.isManagment = isManagment;
        this.hasCriminalRecord = hasCriminalRecord;
        this.hasSexOffenderRecord = hasSexOffenderRecord;
        }

//
//    private LocalDate parseLocalDate(String dateToParse) {
//        LocalDate returnDate = null;
//        if((dateToParse.equals("") == false)){
//            try {
//                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//                //convert String to LocalDate
//                returnDate = LocalDate.parse(dateToParse, formatter);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        return returnDate;
//    }
    //TODO: change to LocalDate, Date is a bad object
    private Date parseDate(String dateToParse) {
        Date returnDate = null;
        if((dateToParse.equals("") == false)){
            try {
                returnDate = (new SimpleDateFormat("yyyy-MM-dd")).parse(dateToParse);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return returnDate;
    }
//
//    @Override
//    public String  toString() {
//        return "Instructor{" +
//                "firstName='" + firstName + '\'' +
//                ", lastName='" + lastName + '\'' +
//                ", idNumber='" + idNumber + '\'' +
//                ", house=" + house +
//                ", gender=" + gender +
//                ", dateOfEmployment=" + dateOfEmployment +
//                ", dateOfBirth=" + dateOfBirth +
//                ", shirtSize=" + shirtSize +
//                ", position=" + position +
//                ", isShantiTherapyInstructor=" + isShantiTherapyInstructor +
//                ", cellphoneNum='" + cellphoneNum + '\'' +
//                ", homePhoneNum='" + homePhoneNum + '\'' +
//                ", eMail='" + eMail + '\'' +
//                ", homeAddress='" + homeAddress + '\'' +
//                ", educationType='" + educationType + '\'' +
//                ", hasALicense=" + hasALicense +
//                ", licenseDescription='" + licenseDescription + '\'' +
//                ", employmentStatus=" + employmentStatus +
//                ", dateOfUnemployment=" + dateOfUnemployment +
//                ", unemploymentReason='" + unemploymentReason + '\'' +
//                ", isManagment=" + isManagment +
//                ", hasCriminalRecord=" + hasCriminalRecord +
//                ", hasSexOffenderRecord=" + hasSexOffenderRecord +
//                '}';
//    }

}
