package com.bethashanti.beckend.objects;

import org.springframework.http.HttpStatus;

import java.util.List;

public class HttpResponse {

    private HttpStatus httpStatus;
    private List<String> messages;
    private Object object;

    public HttpResponse(HttpStatus httpStatus, List<String> messages, Object object) {
        this.httpStatus = httpStatus;
        this.messages = messages;
        this.object = object;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }
}
