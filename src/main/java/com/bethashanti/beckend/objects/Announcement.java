//package com.bethashanti.beckend.objects;
//
//import com.bethashanti.beckend.EnumTypes;
//import com.bethashanti.beckend.objects.Child;
//import com.bethashanti.beckend.objects.Instructor;
//
//import java.text.ParseException;
//import java.util.*;
//import java.text.SimpleDateFormat;
//
//public class Announcement {
//
//    private final long systemId;
//    private Date composedDate;
//    private Instructor composingInstructor;
//    private EnumTypes.Topic topic;
//    private List<Child> releventChildren;
//    private List<Instructor> releventInstructors;
//    private Map<Instructor, String> managmentComments;
//    private String content;
//
//
//    public Announcement(long systemId, String composedDate, String composingInstructor, String topic, List<Child> releventChildren,
//                        List<Instructor> releventInstructors, String content) {
//        this.systemId = systemId;
//        if(composedDate.equals("")) {
//            this.composedDate = null;
//        }
//        else {
//            try {
//                this.composedDate = new SimpleDateFormat("dd/MM/yyyy").parse(composedDate);
//            }
//            catch (ParseException e) {
//                e.printStackTrace();
//            }
//        }
//        //TODO in the main class get instructor
//        //this.composingInstructor = Instructor.getInstructorByName(composingInstructor);
//        this.composingInstructor = null;
//        this.topic = EnumTypes.Topic.valueOf(topic);
//        this.releventChildren = releventChildren;
//        // TODO
//        this.releventInstructors = releventInstructors;
//        this.managmentComments = new HashMap<Instructor, String>();
//        this.content = content;
//    }
//
//    public String getDate() {
//        return this.composedDate.toString();
//    }
//
//    public String getComposingInstructor() {
//        return this.composingInstructor.getFirstName() + " " + this.composingInstructor.getLastName();
//    }
//
//    public String getTopic() {
//        return this.topic.toString();
//    }
//    public void setTopic(String newTopic) {
//        this.topic = EnumTypes.Topic.valueOf(newTopic);
//    }
//
//    public List<String> getReleventChildren() {
//        List<String> children = new ArrayList<String>();
//        for(Child child : releventChildren ) {
//            children.add(child.getFirstName() + " " + child.getLastName());
//        }
//        return children;
//    }
//    ///TODO - create get child by full name
//    //public void setReleventChildren(String childName) {
//    //    this.releventChildren.add()
//    //}
//
//    public List<String> getReleventInstructors() {
//        List<String> instructors = new ArrayList<>();
//        for (Instructor instructor : releventInstructors) {
//            instructors.add(instructor.getFirstName() + " " + instructor.getLastName());
//        }
//        return instructors;
//    }
//
//    //TODO- turn into string
//    public Map<Instructor, String> getManagmentComments() {
//        return this.managmentComments;
//    }
//    public void setManagmentComments(Instructor instructor, String comment) {
//        this.managmentComments.put(instructor, comment);
//    }
//
//    public String getContent() { return this.content; }
//}
