package com.bethashanti.beckend.objects;

import com.bethashanti.beckend.EnumTypes;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@Getter // to use: enable annotation processing
@Setter // to use: enable annotation processing
@Entity
@Table(name = "child")

public class Child {

    @Column(name = "house")
    private EnumTypes.House house;

    @NotNull
    @Id
    @Column(name = "id_number")
    private long idNumber;

    @Past
    @Column(name = "arrival_date")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate arrivalDate;

    @NotNull
    @Column(name = "first_name")
    private String firstName;

    @NotNull
    @Column(name = "last_name")
    private String lastName;

    @Column(name = "gender")
    private EnumTypes.Sex gender;

    @Past
    @Column(name = "date_of_birth")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate dateOfBirth;

    @Column(name = "city")
    private String city;

    @Column(name = "address")
    private String address;

    @Column(name = "instructor")
    //////////// TODO public Instructor staffInCharge;
    private String staffInCharge;

    @Column(name = "status")
    private EnumTypes.Status status;

    @Column(name = "phone_number")
    private String phoneNum;

    @Column(name = "email")
    private String eMail;

    @Column(name = "mothers_name")
    private String mothersName;

    @Column(name = "fathers_name")
    private String fathersName;

    @Column(name = "num_of_siblings")
    private String numOfSiblings;

    @Column(name = "economic_status")
    private EnumTypes.EconomicStatus economicStatus;

    @Column(name = "origin")
    private EnumTypes.Origin origin;

    @Column(name = "nation")
    private EnumTypes.Nation nation;

    @Column(name = "place_of_birth")
    private String placeOfBirth;

    @Column(name = "passport_number")
    private String passportNum;

    @Column(name = "citizenship")
    private String citizenship;

    public Child() {
        this.idNumber = 12312;
        this.firstName = "test1";
        this.lastName = "test2";
        this.phoneNum = "0548050356";
    }

    public Child(EnumTypes.House house, @NotNull long idNumber, LocalDate arrivalDate, String firstName, String lastName, EnumTypes.Sex gender, LocalDate dateOfBirth, String city, String address, String staffInCharge, EnumTypes.Status status, String phoneNum, String eMail, String mothersName, String fathersName, String numOfSiblings, EnumTypes.EconomicStatus economicStatus, EnumTypes.Origin origin, EnumTypes.Nation nation, String placeOfBirth, String passportNum, String citizenship) {
        this.house = house;
        this.idNumber = idNumber;
        this.arrivalDate = arrivalDate;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.city = city;
        this.address = address;
        this.staffInCharge = staffInCharge;
        this.status = status;
        this.phoneNum = phoneNum;
        this.eMail = eMail;
        this.mothersName = mothersName;
        this.fathersName = fathersName;
        this.numOfSiblings = numOfSiblings;
        this.economicStatus = economicStatus;
        this.origin = origin;
        this.nation = nation;
        this.placeOfBirth = placeOfBirth;
        this.passportNum = passportNum;
        this.citizenship = citizenship;
    }

    public EnumTypes.House getHouse() {
        return house;
    }

    public void setHouse(EnumTypes.House house) {
        this.house = house;
    }

    public long getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(long idNumber) {
        this.idNumber = idNumber;
    }

    public LocalDate getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(LocalDate arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public EnumTypes.Sex getGender() {
        return gender;
    }

    public void setGender(EnumTypes.Sex gender) {
        this.gender = gender;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStaffInCharge() {
        return staffInCharge;
    }

    public void setStaffInCharge(String staffInCharge) {
        this.staffInCharge = staffInCharge;
    }

    public EnumTypes.Status getStatus() {
        return status;
    }

    public void setStatus(EnumTypes.Status status) {
        this.status = status;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getMothersName() {
        return mothersName;
    }

    public void setMothersName(String mothersName) {
        this.mothersName = mothersName;
    }

    public String getFathersName() {
        return fathersName;
    }

    public void setFathersName(String fathersName) {
        this.fathersName = fathersName;
    }

    public String getNumOfSiblings() {
        return numOfSiblings;
    }

    public void setNumOfSiblings(String numOfSiblings) {
        this.numOfSiblings = numOfSiblings;
    }

    public EnumTypes.EconomicStatus getEconomicStatus() {
        return economicStatus;
    }

    public void setEconomicStatus(EnumTypes.EconomicStatus economicStatus) {
        this.economicStatus = economicStatus;
    }

    public EnumTypes.Origin getOrigin() {
        return origin;
    }

    public void setOrigin(EnumTypes.Origin origin) {
        this.origin = origin;
    }

    public EnumTypes.Nation getNation() {
        return nation;
    }

    public void setNation(EnumTypes.Nation nation) {
        this.nation = nation;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getPassportNum() {
        return passportNum;
    }

    public void setPassportNum(String passportNum) {
        this.passportNum = passportNum;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }
}




//package com.bethashanti.beckend.objects;
//
//import com.bethashanti.beckend.EnumTypes;
//import org.springframework.stereotype.Component;
//
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.*;
//
//public class Child {
//
//    private final long systemId;
//    private EnumTypes.House house;
//    private String idNumber;
//    private Date arrivalDate;
//    private String firstName;
//    private String lastName;
//    private EnumTypes.Sex gender;
//    private Date dateOfBirth;
//    private String city;
//    private String address;
//    //////////// TODO public Instructor staffInCharge;
//    private String staffInCharge;
//    private EnumTypes.Status status;
//    private String phoneNum;
//    private String eMail;
//    private String mothersName;
//    private String fathersName;
//    //private EnumTypes.ParentsStatus parentsStatus;
//    private String numOfSiblings;
//    private EnumTypes.EconomicStatus economicStatus;
//    private EnumTypes.Origin origin;
//    private EnumTypes.Nation nation;
//    private String placeOfBirth;
//    private String passportNum;
//    private String citizenship;
//    //private String dream;
//    //private String dreamPriority;
//    //private String personalSidur;
//    //private EnumTypes.Restriction restriction1;
//    //private EnumTypes.Restriction restriction2;
//    //private List<EnumTypes.Restriction> restrictions;
//    //private List<Date> endDates;
//    //private Date endDate1;
//    //private Date endDate2;
//    //private Date leaveDate;
//    //private String moveTo;
//    //private String reasonForLeaving;
//
//    public Child() {
//        systemId = 0;
//    }
//
//    public Child(long systemId, EnumTypes.House house, String idNumber, Date arrivalDate, String firstName, String lastName,
//                 EnumTypes.Sex gender, Date dateOfBirth, String city, String address, String staffInCharge, EnumTypes.Status status,
//                 String phoneNum, String eMail, String mothersName, String fathersName,
//                 String numOfSiblings, EnumTypes.EconomicStatus economicStatus, EnumTypes.Origin origin, EnumTypes.Nation nation,
//                 String placeOfBirth, String passportNum, String citizenship) {
//        this.systemId = systemId;
//        this.house = house;
//        this.idNumber = idNumber;
//        this.arrivalDate = arrivalDate;
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.gender = gender;
//        this.dateOfBirth = dateOfBirth;
//        this.city = city;
//        this.address = address;
//        this.staffInCharge = staffInCharge;
//        this.status = status;
//        this.phoneNum = phoneNum;
//        this.eMail = eMail;
//        this.mothersName = mothersName;
//        this.fathersName = fathersName;
//        this.numOfSiblings = numOfSiblings;
//        this.economicStatus = economicStatus;
//        this.origin = origin;
//        this.nation = nation;
//        this.placeOfBirth = placeOfBirth;
//        this.passportNum = passportNum;
//        this.citizenship = citizenship;
//    }
//
//    public long getSystemId() {
//        return systemId;
//    }
//
//    public EnumTypes.House getHouse() {
//        return house;
//    }
//
//    public void setHouse(EnumTypes.House house) {
//        this.house = house;
//    }
//
//    public String getIdNumber() {
//        return idNumber;
//    }
//
//    public void setIdNumber(String idNumber) {
//        this.idNumber = idNumber;
//    }
//
//    public Date getArrivalDate() {
//        return arrivalDate;
//    }
//
//    public void setArrivalDate(Date arrivalDate) {
//        this.arrivalDate = arrivalDate;
//    }
//
//    public String getFirstName() {
//        return firstName;
//    }
//
//    public void setFirstName(String firstName) {
//        this.firstName = firstName;
//    }
//
//    public String getLastName() {
//        return lastName;
//    }
//
//    public void setLastName(String lastName) {
//        this.lastName = lastName;
//    }
//
//    public EnumTypes.Sex getGender() {
//        return gender;
//    }
//
//    public void setGender(EnumTypes.Sex gender) {
//        this.gender = gender;
//    }
//
//    public Date getDateOfBirth() {
//        return dateOfBirth;
//    }
//
//    public void setDateOfBirth(Date dateOfBirth) {
//        this.dateOfBirth = dateOfBirth;
//    }
//
//    public String getCity() {
//        return city;
//    }
//
//    public void setCity(String city) {
//        this.city = city;
//    }
//
//    public String getAddress() {
//        return address;
//    }
//
//    public void setAddress(String address) {
//        this.address = address;
//    }
//
//    public String getStaffInCharge() {
//        return staffInCharge;
//    }
//
//    public void setStaffInCharge(String staffInCharge) {
//        this.staffInCharge = staffInCharge;
//    }
//
//    public EnumTypes.Status getStatus() {
//        return status;
//    }
//
//    public void setStatus(EnumTypes.Status status) {
//        this.status = status;
//    }
//
//    public String getPhoneNum() {
//        return phoneNum;
//    }
//
//    public void setPhoneNum(String phoneNum) {
//        this.phoneNum = phoneNum;
//    }
//
//    public String geteMail() {
//        return eMail;
//    }
//
//    public void seteMail(String eMail) {
//        this.eMail = eMail;
//    }
//
//    public String getMothersName() {
//        return mothersName;
//    }
//
//    public void setMothersName(String mothersName) {
//        this.mothersName = mothersName;
//    }
//
//    public String getFathersName() {
//        return fathersName;
//    }
//
//    public void setFathersName(String fathersName) {
//        this.fathersName = fathersName;
//    }
//
//    public String getNumOfSiblings() {
//        return numOfSiblings;
//    }
//
//    public void setNumOfSiblings(String numOfSiblings) {
//        this.numOfSiblings = numOfSiblings;
//    }
//
//    public EnumTypes.EconomicStatus getEconomicStatus() {
//        return economicStatus;
//    }
//
//    public void setEconomicStatus(EnumTypes.EconomicStatus economicStatus) {
//        this.economicStatus = economicStatus;
//    }
//
//    public EnumTypes.Origin getOrigin() {
//        return origin;
//    }
//
//    public void setOrigin(EnumTypes.Origin origin) {
//        this.origin = origin;
//    }
//
//    public EnumTypes.Nation getNation() {
//        return nation;
//    }
//
//    public void setNation(EnumTypes.Nation nation) {
//        this.nation = nation;
//    }
//
//    public String getPlaceOfBirth() {
//        return placeOfBirth;
//    }
//
//    public void setPlaceOfBirth(String placeOfBirth) {
//        this.placeOfBirth = placeOfBirth;
//    }
//
//    public String getPassportNum() {
//        return passportNum;
//    }
//
//    public void setPassportNum(String passportNum) {
//        this.passportNum = passportNum;
//    }
//
//    public String getCitizenship() {
//        return citizenship;
//    }
//
//    public void setCitizenship(String citizenship) {
//        this.citizenship = citizenship;
//    }
//}
