package com.bethashanti.beckend.services;

import com.bethashanti.beckend.dao.UserDao;
import com.bethashanti.beckend.objects.User;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class UserInitService implements CommandLineRunner {
    private UserDao userDao;
    private PasswordEncoder passwordEncoder;

    public UserInitService(UserDao userDao, PasswordEncoder passwordEncoder) {
        this.userDao = userDao;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) {
        // Delete all
        this.userDao.deleteAll();

        // Crete users
        User user = new User("user", passwordEncoder.encode("user123"),"USER","");
        User admin = new User("admin", passwordEncoder.encode("admin123"),"ADMIN","ACCESS_TEST1,ACCESS_TEST2");
        User manager = new User("manager", passwordEncoder.encode("manager123"),"MANAGER","ACCESS_TEST1");

        List<User> users = Arrays.asList(user,admin,manager);

        // Save to db
        this.userDao.saveAll(users);
    }
}
