//package com.bethashanti.beckend.services;
//
//import com.bethashanti.beckend.dao.ChildDao;
//import com.bethashanti.beckend.objects.Child;
//import com.bethashanti.beckend.objects.ChildTest;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//
//@Component
//@Service
//public class ChildService {
//
//    @Autowired
//    ChildDao childDao;
//
//    public ChildService() {
//        //this.childDao = new ChildDao();
//    }
//
//    public boolean checkIfChildExists(Child child) {
//        return childDao.checkIfExsits(child.getIdNumber());
//    }
//
//    public boolean saveChild(Child child) {
//        return childDao.save(child);
//    }
//
//
//    public boolean deleterChile(String id) {
//        return childDao.delete(id);
//    }
//
//    public List<Child> getChildren() {
//        return childDao.getChildren();
//    }
//
//    public void saveChildTest(ChildTest childTest) {
//        childDao.saveChildTest(childTest);
//    }
//}
package com.bethashanti.beckend.services;

import com.bethashanti.beckend.dao.ChildDao;
import com.bethashanti.beckend.dao.InstructorDao;
import com.bethashanti.beckend.objects.Child;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ChildService {

    @Autowired
    ChildDao childDao;

    public ChildService(){

    }

    /**
     * saves child to repository.
     * if child id already exists, override it
     * @param child - object to store in repository
     */
    public Child saveChild(Child child) {
        return childDao.save(child);

    }

    /**
     * returns child from the repository by a given id
     * @param id - the children id to retrieve by id
     * @return an child object from repository or null if doesnt exist
     */
    public Optional<Child> getChildById(Long id) {
        return childDao.findById(id);
    }

    /**
     * returns a list of all children in the repository
     * @return list of children
     */
    public Iterable<Child> getAllChild() {
        return childDao.findAll();
    }

    /**
     * deletes an child from the repository by id number
     * @param id the id number of the child to be deleted
     */
    public void deleteChild (Long id){
        childDao.deleteById(id);
    }
}
