package com.bethashanti.beckend.services;

import com.bethashanti.beckend.dao.InstructorDao;
import com.bethashanti.beckend.objects.Instructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InstructorService {

    @Autowired
    InstructorDao instructorDao;

    public InstructorService(){

    }

    /**
     * saves instructor to repository.
     * if instructor id already exists, override it
     * @param instructor - object to store in repository
     */
    public void saveInstructor(Instructor instructor) {
         instructorDao.save(instructor);

    }

    /**
     * returns instructor from the repository by a given id
     * @param id - the instructor id to retrieve by id
     * @return an Instructor object from repository or null if doesnt exist
     */
    public Optional<Instructor> getInstructorById(Long id) {
       return instructorDao.findById(id);
    }

    /**
     * returns a list of all instructors in the repository
     * @return list of instructors
     */
    public Iterable<Instructor> getAllInstructors() {
        return instructorDao.findAll();
    }

    /**
     * deletes an instructor from the repository by id number
     * @param id the id number of the instructor to be deleted
     */
    public void deleteInstructor (Long id){
        instructorDao.deleteById(id);
    }
}
