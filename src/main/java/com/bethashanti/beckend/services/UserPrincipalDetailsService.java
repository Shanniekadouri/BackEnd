package com.bethashanti.beckend.services;

import com.bethashanti.beckend.dao.UserDao;
import com.bethashanti.beckend.objects.User;
import com.bethashanti.beckend.objects.UserPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserPrincipalDetailsService implements UserDetailsService {
    private UserDao userRepository;

    public UserPrincipalDetailsService(UserDao userRepository){
        this.userRepository = userRepository;
    }


    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        User user = this.userRepository.findByUsername(userName);
        UserPrincipal userPrincipal = new UserPrincipal(user);
        return userPrincipal;
    }
}
