package com.bethashanti.beckend.connector;

import com.mysql.cj.jdbc.MysqlDataSource;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
public class Connector {

    DataSource dataSource;

    public Connector() {
        MysqlDataSource mysqlDataSource = new MysqlDataSource();
        mysqlDataSource.setURL("jdbc:mysql://localhost:3306/mysql");
        mysqlDataSource.setUser("root");
        mysqlDataSource.setPassword("0548050356");
        //mysqlDataSource.setPassword("Dean654123");
        this.dataSource = mysqlDataSource;
    }

    public DataSource getDataSource() {
        return dataSource;
    }
}
